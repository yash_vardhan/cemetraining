package com.conygre.lab;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class CompactDisc {
    public CompactDisc(int id, String title, String artist, String price) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.price = price;
    }

    public CompactDisc() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    private String artist;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    private String price;
}
