package com.conygre.test.pets;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        
       // ApplicationContext context=new AnnotationConfigApplicationContext(PetConfigurer.class);
        /*
           
             com.conygre.test.pets.Owner owner=context.getBean(com.conygre.test.pets.Owner.class);
             owner.getPet().feed();
             owner.getPet2().feed();
        
     */

        {
            ApplicationContext context2=new ClassPathXmlApplicationContext("classpath*:pets.xml");
            context2.getBean(Owner.class).getPet().feed();
        }
    
    }
    
}
