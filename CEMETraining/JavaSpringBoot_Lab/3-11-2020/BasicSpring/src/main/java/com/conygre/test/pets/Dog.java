package com.conygre.test.pets;

import org.springframework.stereotype.Component;

//@Component("Dog")
public class Dog implements Pet {
    
    public void feed() {
        System.out.println("feed dog");
    }
}
