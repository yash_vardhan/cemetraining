package com.allstate.entities;

public class Employee extends person {
    private String email;
    private double Salary;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSalary() {
        return Salary;
    }

    public void setSalary(double salary) {
        Salary = salary;
    }

    public Employee(String email, double salary) {
        this.email = email;
        Salary = salary;
    }

    public Employee(int id, String n, int age, String email, double salary) {
        super(id, n, age);
        this.email = email;
        Salary = salary;
    }
    @Override
    public void display()
    {
        System.out.printf("Values are %s ,%d", this.getName(),this.getId(),this.Salary,this.email);
    }

}
