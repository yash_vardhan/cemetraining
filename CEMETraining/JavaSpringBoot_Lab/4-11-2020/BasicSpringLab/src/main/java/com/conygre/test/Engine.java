package com.conygre.test;

public interface Engine {
    double getEngineSize();
    void setEngineSize(double enginesize);
    
}
