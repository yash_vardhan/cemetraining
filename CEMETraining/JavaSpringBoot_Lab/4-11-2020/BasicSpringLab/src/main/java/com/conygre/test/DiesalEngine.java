package com.conygre.test;

import org.springframework.stereotype.Component;

@Component
public class DiesalEngine implements Engine {
    public double EngineSize=4;

    public double getEngineSize()
    {
        return this.EngineSize;
    }

    public void setEngineSize(double enginesize)
    {
        this.EngineSize=enginesize;
    }
    
}
