package com.conygre.test;

import org.springframework.stereotype.Component;

@Component
public class PetrolEngine implements Engine {
    private double engineSize=3.3;

    @Override
    public double getEngineSize()
    {
        return this.engineSize;
    }
     @Override
     public void setEngineSize(double enginesize)
     {
           this.engineSize=enginesize;
     }
}
