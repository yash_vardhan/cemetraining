package com.allstate.labs;

public abstract class Account {
    private double balance;
    private String name;
    private static double intrestRate;

    public Account(){}

    public Account(double balance,String name)
    {
        this.balance=balance;
        this.name=name;
    }
 
    public void setBalance(double balance)
    {
      this.balance=balance;
    }
    public double getBalalnce()
    {
        return balance;
    }
    public void setName(String name)
    {
      this.name=name;
    }
    public String getName()
    {
      return name;
    }
    public void addIntrest()
    {
      
        double IncreasedIntrest=balance+(balance*10/100)+intrestRate;
        System.out.println(name+" "+"Your Total amount after intrest is"+" "+IncreasedIntrest);
     
    }
    public abstract void addIntrestusingoverride();
  
        
    public void  addMultipleIntrests(Account[] accounts)
    {
      if(intrestRate > 0)
      {
        for(Account ac:accounts)
      {
         double intrests=ac.balance+(ac.balance*10/100)+intrestRate;
         System.out.println(ac.name+"has New Total Intrest Of"+" "+intrests);
      }
      }
      else{
        for(Account ac:accounts)
      {
         double intrests=ac.balance+(ac.balance*10/100);
         System.out.println(ac.name+"has Total Intrest Of"+" "+intrests);
      }
      }
    }

    public static double getIntrestRate() {
      return intrestRate;
    }

    public static void setIntrestRate(double intrestrate)
     {
       intrestRate= intrestrate;
     }
}
