package com.allstate.entities;

public class person {
    private int id;
    private String name;
    private int age;
    private static int ageLimit;

    static{
       ageLimit=105;
    }
    public person()
    {
        this.id=0;
    }
    public person(int id,String n,int age)
    {
       this.id=id;
       this.name=n;
       this.age=age;
    }
    public int getId()
    {
        return this.id;
    }
    public void setId(int id)
    {
        this.id=id;
    }
    public static int getAgeLimit()
    {
          return ageLimit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void display()
    {
        System.out.printf("Values are %s ,%d", this.name,this.id);
    }
    protected void print()
    {
        System.out.printf("Printed values are %s,%d",this.name,this.id);
    }
    public static void printf()
    {
        System.out.println("This is base class method");
    }
}
