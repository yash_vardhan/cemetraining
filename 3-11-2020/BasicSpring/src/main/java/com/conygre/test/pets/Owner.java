package com.conygre.test.pets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

//@Component
public class Owner {
   // @Autowired
    //@Qualifier("Cat")
    private Pet pet;
    //@Autowired
    //@Qualifier("Dog")
    private Pet pet2;

    public Owner(Pet p1,Pet p2 ) {
        this.pet = p1;

        this.pet2=p2;
    }
    public Owner(Pet p)
    {
        this.pet=p;
    }
    public Owner(){}

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet,Pet pet2) {
        this.pet = pet;
        this.pet2=pet2;
    }
    public Pet getPet2(){
        return pet2;
    }
    
}
